import java.util.Arrays;
import java.util.Scanner;

public class Solution {

    private static final Scanner sc = new Scanner(System.in);

    public static void main(String[] args){
        int[] sequenceOfIntegers = getSequence();
        findMaxLength(sequenceOfIntegers);
        findMinLength(sequenceOfIntegers);
        findIntegerWithMinimumDifferentDigits(sequenceOfIntegers);
        findIntegerInAscendingOrder(sequenceOfIntegers);
        findIntegerWithOnlyDifferentDigits(sequenceOfIntegers);
    }

    //Заполнение массива последовательностью целых чисел
    public static int[] getSequence(){
        System.out.println("Please enter integer n. Where n is a quantity of integers to type later");
        int n = sc.nextInt();
        if (n <= 0) {
            System.out.println("Error. Integer n must be greater than 0.");
            return new int[0];
        } else {
            int[] array = new int[n];
            System.out.println("Please enter integers:");
            for (int i = 0; i < n; i++) {
                array[i] = sc.nextInt();
            }
            return array;
        }
    }

    //Подсчет количества знаков в числе
    public static int getCountsOfDigits(int number){
        int count = (number == 0) ? 1 : 0;
        while (number != 0) {
            count++;
            number /= 10;
        }
        return count;
    }

    //Подсчет разных знаков в числе
    public static int getCountOfDifferentDigits(int num) {
        String word = Integer.toString(num);
        char[] intChars = word.toCharArray();
        int result = 0;
        for (int i = 0; i < intChars.length; i++) {
            if (intChars[0] != intChars[i]) {
                result++;
            }
        }
        return result;
    }

    //Подсчет повторяющихся знаков в числе
    public static int getCountOfOnlyDifferentDigits(int num) {
        String word = Integer.toString(num);
        char[] intChars = word.toCharArray();
        Arrays.sort(intChars);
        int result = 0;
        for (int i = 0; i < intChars.length; i++) {
            for (int j = i + 1; j < intChars.length; j++){
                if (intChars[i] == intChars[j]){
                    result++;
                }
            }
        }
        return result;
    }

    //Определение порядке возрастания знаков в числе
    public static boolean isIntegerInAscOrder(int num) {
        String word = Integer.toString(num);
        char[] intChars = word.toCharArray();
        char[] tempArray = Arrays.copyOfRange(intChars, 0, intChars.length);
        Arrays.sort(tempArray);
        if (Arrays.equals(tempArray, intChars)) {
            return true;
        }
        else {
            return false;
        }
    }

    //Поиск максимальной длины числа
    public static void findMaxLength(int[] array){
        int maxLength = getCountsOfDigits(array[0]);
        int maxInt = array[0];
        for (int i = 0; i < array.length; i++){
            if (maxLength < getCountsOfDigits(array[i])){
                maxLength = getCountsOfDigits(array[i]);
                maxInt = array[i];
            }
        }
        System.out.println("The longest integer is " + maxInt + " with a maximum length of " + maxLength);
    }

    //Поиск минимальной длины числа
    public static void findMinLength(int[] array){
        int minLength = getCountsOfDigits(array[0]);
        int minInt = array[0];
        for (int i = 0; i < array.length; i++){
            if (minLength > getCountsOfDigits(array[i])){
                minLength = getCountsOfDigits(array[i]);
                minInt = array[i];
            }
        }
        System.out.println("The shortest integer is " + minInt + " with a minimum length of " + minLength);
    }

    //Поиск числа с минимальным количеством разных знаков
    public static void findIntegerWithMinimumDifferentDigits(int[] array){
        int minDif = array[0];
        for (int i = 0; i < array.length; i++){
            if (getCountOfDifferentDigits(minDif) > getCountOfDifferentDigits(array[i])){
                minDif = array[i];
            }
        }
        System.out.println("Integer with the least different digits: " + minDif);
    }

    //Поиск числа, знаки которого находятся в строгом порядке возрастания
    private static void findIntegerInAscendingOrder(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (isIntegerInAscOrder(array[i]) == true){
                System.out.println("Integer in ascending order: " + array[i]);
                break;
            }
        }
    }

    //Поиск числа, в котором только разные знаки
    public static void findIntegerWithOnlyDifferentDigits(int[] array){
        int onlyDiffDigits = array[0];
        for (int i = 0; i < array.length; i++){
            if (getCountOfOnlyDifferentDigits(array[i]) == 0) {
                onlyDiffDigits = array[i];
                break;
            }
        }
        System.out.println("Integer with only different digits: " + onlyDiffDigits);
    }
}